import torch as T
from torch import nn
from modules import Sender
from modules import Receiver


class Game(nn.Module):
    def __init__(self, vocab_size, image_dim, s_embd_dim, s_hid_dim, r_embd_dim, r_hid_dim, bound_idx, max_steps, tau, straight_through):
        super().__init__()
        self.sender = Sender(vocab_size, s_embd_dim, s_hid_dim, image_dim, bound_idx, max_steps, tau, straight_through)
        self.receiver = Receiver(vocab_size, r_embd_dim, r_hid_dim, image_dim)

    def forward(self, image_f, margin, greedy=False):
        device = image_f.device
        entropy, x = self.sender(image_f, greedy)
        predicted_f = self.receiver(x)
        scores = T.matmul(image_f, predicted_f.permute(1, 0))

        pred_idxs = T.argmax(scores, dim=1, keepdim=True)
        diag_idxs = T.tensor([[i] for i in range(image_f.shape[0])], device=device)
        accuracy = (pred_idxs == diag_idxs).to(dtype=T.float32)

        hinge_loss = T.max(margin - T.diagonal(scores)[:, None] + scores, other=T.tensor(0.0, device=device))
        hinge_loss.scatter_(-1, diag_idxs, 0.0)

        return T.mean(T.sum(hinge_loss, dim=1)), T.mean(accuracy), T.mean(entropy)
