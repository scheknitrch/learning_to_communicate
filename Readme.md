1. run `python data_preprocessing/preprocess_data.py` to generate train/test/valid split.
2. run `python train_tb.py` script to train the model.
3. run `python test_tb.py` script to test the model.
4. run `python generate_test_captions.py` script to generate captions