import numpy as np


class ImageDataset(object):
    def __init__(self, features, mean=None, std=None):
        if mean is None:
            mean = np.mean(features, axis=0)
            std = np.std(features, axis=0)
            std[np.nonzero(std == 0.0)] = 1.0  # nan is because of dividing by zero
        self.mean = mean
        self.std = std
        self.features = (features - self.mean) / (2 * self.std)

    def __getitem__(self, index):
        return self.features[index]

    def __len__(self):
        return self.features.shape[0]