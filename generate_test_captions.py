import json
import pickle
import torch as T
import numpy as np
from modules import Game
from torch.utils.data import DataLoader
from data_preprocessing import ImageDataset


gpu_id = 1
device = T.device("cuda:{}".format(gpu_id))

with open("data/mscoco/dict.pckl", "rb") as f:
    d = pickle.load(f)
    vocab_size = len(d["word_to_idx"])
    bound_idx = d["word_to_idx"]["<S>"]
train_features = np.load('data/mscoco/train_features.npy')
test_features = np.load('data/mscoco/test_features.npy')
with open('data/mscoco/test_captions.json') as f:
    test_captions = json.load(f)




max_steps = 13
checkpoint = T.load("data/models/tb/m-7379546931557357604/75.mdl")
model = Game(vocab_size=vocab_size, image_dim=test_features.shape[1],
             s_embd_dim=256, s_hid_dim=512, r_embd_dim=256, r_hid_dim=512,
             bound_idx=bound_idx, max_steps=max_steps, tau=1.2, straight_through=True).cuda(gpu_id)
model.load_state_dict(checkpoint["state_dict"])
model.eval()


train_dataset = ImageDataset(train_features)
test_dataset = ImageDataset(test_features, mean=train_dataset.mean, std=train_dataset.std)
test_data = DataLoader(test_dataset, shuffle=False, batch_size=128, num_workers=8, pin_memory=True)


N = 10
generated_captions = {}
for i, image_f in enumerate(test_data):
    batch_captions = test_captions[i * 128:(i+1) * 128]
    image_f = image_f.to(device=device)
    x = [model.sender(image_f)[1][:, 1:] for k in range(N)]
    for j in range(len(batch_captions)):
        path = batch_captions[j][0].split("datasets/")[1]
        generated_captions[path] = [x[k][j].data.tolist() for k in range(N)]
with open(f"data/generated_captions{max_steps}.json", 'w') as f:
    json.dump(generated_captions, f)